/*
  Challege 1 | Find Smallest Item in Array
*/
var list = [5, 3, 7, 2, 6, 8, 1, 4];

Array.min = function(list) {
  return Math.min.apply(Math, list);
};

var minimum = Array.min(list);
console.log(minimum);

/*
  Challege 2 | Find Highest Item in Array
*/
var list = [5, 3, 7, 2, 6, 8, 1, 4];

Array.max = function(list) {
  return Math.max.apply(Math, list);
};

var minimum = Array.max(list);
console.log(minimum);

/*
  Challege 3 | Function to Return the Sum of Two Numbers
*/
// Syntax Arrow Function
const sumOfNum = (x, y) => {
  return x + y;
}

var result1 = sumOfNum(7, 6);
console.log(result1);

/*
  Challege 4 | Function to Return the Next Number from the Integer Passed
*/
// Syntax Arrow Function
const nextNum = (x) => {
  return x + 1;
}

var result2 = nextNum(10);
console.log(result2);

/*
  Challege 5 | Function to Return, Is the Number Less than or Equal to Zero?
*/
// Syntax Arrow Function
const lessThanOrEqualToZero = (x) => {
  if (x <= 0) {
    return true;
  } else {
    return false;
  }
}

var result3 = lessThanOrEqualToZero(0);
console.log(result3);

/*
  Challege 6 | Function to Return, Compare Strings by Sum of Characters
*/
// Syntax Arrow Function
const sumOfChars = (string1, string2) => {
  if (string1.length == string2.length) {
    return true;
  } else {
    return false;
  }
}

var result4 = sumOfChars('HIB', 'FB');
console.log(result4);

/*
  Challege 7 | Function to Return, Return the Last Item in an Array
*/
// Syntax Arrow Function
const lastItem = (array) => {
  return array[array.length - 1];
}

var result5 = lastItem([2, 5, 'hi', 9, false]);
console.log(result5);

/*
  Challege 8 | Function to Return, Whether Number is Even or Odd
*/
// Syntax Arrow Function
const oddOrEven = (x) => {
  if (x % 2 === 0) {
    return 'Number is even.';
  } else {
    return 'Number is odd.';
  }
}

var result6 = oddOrEven(9);
console.log(result6);

/*
  Challege 9 | Function to Return Product of a Number to the Power
*/
// Syntax Arrow Function
const calcExponent = (x, y) => {
  if (x > 0 && y > 0) {
    return Math.pow(x, y);
  } else {
    return 'Intergers must be positive numbers!';
  }
}

var result7 = calcExponent(2, 10);
console.log(result7);

/*
  Challege 10 | Function to Return Month of the Year According to Number Passed
*/
// Syntax Arrow Function
const monthNum = (num) => {
  let monthsArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  let month = monthsArray[num -1];
  return month;
}

var result8 = monthNum(5);
console.log(result8);

/*
  Challege 11 | Function that Returns the Index Number of an Item in Array
*/
// Syntax Arrow Function
const findIndex = (arr, item) =>{
    if (arr.includes(item)) {
        return arr.indexOf(item);  
    } else {
        return 'Passed value does not exist!';
    }
}

var result9 = findIndex(['blue', 'red', 'purple', 'black'], 'red');
console.log(result9);


/*
  Challege 12 | Function that Returns the Amount of Numbers in Number 
*/
// Syntax Arrow Function
const findDigitAmount = (x) => {
    if (Number.isInteger(x)) {
        return x.toString().length;
    } else {
        return 'Passed parameter must be a number!';
    }
}

var result10 = findDigitAmount(22);
console.log(result10);

/*
  Challege 13 | Function that Returns the Amount of Words in String 
*/
// Syntax Arrow Function
const countWords = (str) => {
    return str.split(' ').length;
} 

var result11 = countWords('Hello my name is Isaac');
console.log(result11);

/*
  Challege 14 | Function that Returns the Reverse of the Boolean Passed 
*/
// Syntax Arrow Function
const reverseBool = (bool) => {
    if (bool === true) {
        return false;
    } else if (bool === false) {
        return true;
    } else {
        return 'Must pass a true or false boolean.';
    }
}

var result12 = reverseBool(true);
console.log(result12);

/*
  Challege 15 | Function that Returns a Name Shuffled
*/
// Syntax Arrow Function
const nameShuffle = (name) => {
    if (typeof name === 'string') {
        return name.split(' ').reverse().join(' ');
    } else {
        return 'Passed parameter must be a string.'
    }
}

var result13 = nameShuffle('Isaac Medina');
console.log(result13);

/*
  Challege 16 | Function that Returns the Amount of Argument
*/
// Syntax Arrow Function
const numArgs = (...args) => {
    return args.length;
}

var result14 = numArgs(true, false);
console.log(result14);

/*
  Challege 17 | Function that Returns a String Reversed
*/
// Syntax Arrow Function
const reverseStr = (str) => {
    return str.split('').reverse().join('');
}

var result15 = reverseStr('I am awesome.');
console.log(result15);

/*
  Challege 18 | Function that Returns Array Item That Has Four Letters
*/
// Syntax Arrow Function
const isFourLetters = (arr) => {
    return arr.filter(i => i.length === 4);
}

var result16 = isFourLetters(['hello', 'bye', 'noun']);
console.log(result16);

/*
  Challege 19 | Function that Returns String Length Into Hyphens
*/
// Syntax Arrow Function
const strHyphen = (num) => {
    if (num <= 60) {
        return '-'.repeat(num);
    } else {
        return 'Number is bigger than 60!.';
    }
}

var result17 = strHyphen(17);
console.log(result17);